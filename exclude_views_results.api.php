<?php

/**
 * @file exclude_views_results.api.php
 * Hooks provided by exclude_views_results API.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Gather views/display to exclude results.
 *
 * @return
 *   An array of Views to group :
 *     - "VIEW_NAME/VIEW_DISPLAY" requiered, array of groups
 *       - "GROUP_NAME"
 *
 */
function hook_exclude_views_results() {
  $items = array();

  /*
   * In this exemple
   *  - my_view_1/my_display_1, my_view_1/my_display_2 and my_view_2/my_display_3 are in "my_group_1"
   *  - my_view_1/my_display_1 and my_view_2/my_display_3 are in "my_group_2"
   *
   * In a group, each view exclude results from views already executed in the same group
   */


  $items = array(
    'my_view_1/my_display_1' => array(
      'my_group_1',
      'my_group_2',
    ),
    'my_view_1/my_display_2' => array(
      'my_group_1',
    ),
    'my_view_2/my_display_3' => array(
      'my_group_1',
      'my_group_2',
    ),
  );

  return $items;
}

/**
 * @} End of "addtogroup hooks".
 */

