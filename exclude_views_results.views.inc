<?php

/**
 * Implements Hook_views_data_alter().
 */
function exclude_views_results_views_data_alter(&$data) {
  $data['node']['nid']['filter']['handler'] = 'views_handler_filter_numeric_not_in';
}

